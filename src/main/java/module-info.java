module ntnu.idatt2001 {
    requires javafx.controls;

    exports ntnu.idatt2001.controller;
    exports ntnu.idatt2001.model;
    exports ntnu.idatt2001.view;
}