package ntnu.idatt2001.view;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ntnu.idatt2001.model.GUIFactory;

import java.io.File;

/**
 * This class is used to make the GUI for save or load data options from the file menu
 */
public class SaveLoadDialog extends Dialog<String> {

    public enum Mode {
        SAVE, LOAD
    }

    private Stage fileChooserStage = new Stage();
    private GUIFactory guiFactory = new GUIFactory();
    private final Mode mode;

    /**
     * The constructor is used to create the GUI window for save or load data options
     * @param mode the mode that we want to work with (SAVE or LOAD)
     */
    public SaveLoadDialog(Mode mode){
        super();
        this.mode = mode;
        createSaveOrLoadDataDialog();
    }

    /**
     * makes the GUI for save or load data options
     */
    private void createSaveOrLoadDataDialog(){
        setTitle("Exporting to .CSV file");
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        GridPane grid = (GridPane) guiFactory.getNode("gridpane");
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        Label fileN = (Label) guiFactory.getNode("label");
        fileN.setText("File Name: ");
        Label loc = (Label) guiFactory.getNode("label");
        loc.setText("Location:  ");

        TextField fileName = (TextField) guiFactory.getNode("Textfield");
        fileName.setPromptText("File Name");

        TextField location = (TextField) guiFactory.getNode("Textfield");
        location.setPromptText("Location");

        grid.add(fileN, 0, 0);
        grid.add(fileName, 1, 0);

        grid.add(loc, 0, 1);
        grid.add(location, 1, 1);

        FileChooser fileChooser = new FileChooser();
        Button selectFile = (Button) guiFactory.getNode("button");
        selectFile.setText(" Select File ");

        //allows the user to choose a file from their computer's file explorer
        selectFile.setOnAction(event -> {
            File selectedFile = fileChooser.showOpenDialog(fileChooserStage);
            if (selectedFile != null){
                fileName.setText(selectedFile.getName());
                location.setText(selectedFile.getParent());
            }
        });

        //extra GUI elements that will be added to the window if the mode is LOAD
        if (mode == Mode.LOAD){
            setTitle("Importing from .CSV file");
            grid.add(selectFile, 0, 2);
        }

        getDialogPane().setContent(grid);


        setResultConverter((ButtonType button) -> {
            String result = "";
            if (button == ButtonType.OK || button.equals(selectFile)){
                if (mode == Mode.SAVE || mode == Mode.LOAD){
                    result = fileName.getText() + ";" + location.getText();
                }
            } else if (button == ButtonType.CANCEL){
                result = null;
            }
            return result;
        });
    }


}
