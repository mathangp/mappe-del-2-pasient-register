package ntnu.idatt2001.view;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ntnu.idatt2001.model.GUIFactory;
import ntnu.idatt2001.model.Patient;

/***
 * This class is used to make the custom menubar, toolbar and tableview that the PatientRegisterApp uses
 */
public class VisualGUI {

    private GUIFactory guiFactory = new GUIFactory();

    /**
     * This method is used to create a custom menubar, toolbar or tableview
     * @param type
     * @return menubar or toolbar or tableview depending in the type given as parameter
     *          return null if type does not match the given choices
     */
    public Node getNode(String type){
        if (type == null){
            return null;
        }

        if (type.equalsIgnoreCase("custom menubar")){
            return menuBar();
        }
        if (type.equalsIgnoreCase("custom toolbar")){
            return toolBar();
        }
        if (type.equalsIgnoreCase("custom tableview")){
            return tableView();
        }

        return null;
    }

    /**
     * The method makes a tableview with the columns that will show the first name, last name,
     * social security number, diagnose and general practitioner of a patient.
     * @return tableview
     */
    private TableView tableView(){
        TableView<Patient> tableView = (TableView<Patient>) guiFactory.getNode("tableview");

        TableColumn<Patient, String> firstName = new TableColumn<>("First Name");
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Patient, String> lastName = new TableColumn<>("Last Name");
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Patient, String> socialSecurityNumber = new TableColumn<>("Social Security Number");
        socialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        TableColumn<Patient, String> diagnose = new TableColumn<>("Diagnose");
        diagnose.setCellValueFactory(new PropertyValueFactory<>("diagnose"));

        TableColumn<Patient, String> generalPractitioner = new TableColumn<>("General Practitioner");
        generalPractitioner.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        tableView.getColumns().addAll(firstName, lastName, socialSecurityNumber, diagnose, generalPractitioner);

        return tableView;
    }

    /**
     * This method makes a custom menubar
     * @return menubar
     */
    private MenuBar menuBar(){
        MenuBar menuBar = (MenuBar) guiFactory.getNode("menubar");

        //The menu bar has the options "File", "Edit", and "Help"
        Menu menuFile = new Menu("File");
        Menu menuEdit = new Menu("Edit");
        Menu menuHelp = new Menu("Help");

        //The "File" option has the following options: "Import from CSV", "Export to CSV" and "Exit"
        MenuItem fileImport = new MenuItem("Import from .CSV / Load Data");
        MenuItem fileExport = new MenuItem("Export to .CSV / Save Data");
        MenuItem fileExit = new MenuItem("Exit");

        menuFile.getItems().addAll(fileImport, fileExport, new SeparatorMenuItem(), fileExit);

        //The "Edit" option has the following options: "Add new patient", "Edit selected patient" and
        //                                              "Remove selected patient"
        MenuItem editAdd = new MenuItem("Add new patient...");
        MenuItem editEdit = new MenuItem("Edit selected patient");
        MenuItem editRemove = new MenuItem("Remove selected patient");

        menuEdit.getItems().addAll(editAdd, editEdit, editRemove);

        //The "Help" option has the following options: "About"
        MenuItem helpAbout = new MenuItem("About");
        menuHelp.getItems().add(helpAbout);

        menuBar.getMenus().addAll(menuFile, menuEdit, menuHelp);

        return menuBar;
    }

    /**
     * This method adds a custom toolbar
     * @return toolbar
     */
    private ToolBar toolBar() {
        ToolBar toolBar = (ToolBar) guiFactory.getNode("toolbar");

        ImageView add = (ImageView) guiFactory.getNode("imageview");
        add.setImage(new Image("add patient.png"));
        add.setFitHeight(30);
        add.setFitWidth(30);

        ImageView edit = (ImageView) guiFactory.getNode("imageview");
        edit.setImage(new Image("edit patient.png"));
        edit.setFitHeight(30);
        edit.setFitWidth(30);

        ImageView remove = (ImageView) guiFactory.getNode("imageview");
        remove.setImage(new Image("remove patient.png"));
        remove.setFitHeight(30);
        remove.setFitWidth(30);


        Button addPatient = (Button) guiFactory.getNode("button");
        addPatient.setGraphic(add);

        Button editPatient = (Button) guiFactory.getNode("button");
        editPatient.setGraphic(edit);

        Button removePatient = (Button) guiFactory.getNode("button");
        removePatient.setGraphic(remove);

        toolBar.getItems().addAll(addPatient, editPatient, removePatient);

        return toolBar;
    }

}
