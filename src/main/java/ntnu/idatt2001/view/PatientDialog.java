package ntnu.idatt2001.view;

import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import ntnu.idatt2001.model.GUIFactory;
import ntnu.idatt2001.model.Patient;

public class PatientDialog extends Dialog<Patient> {

    public enum Mode {
        NEW, EDIT
    }

    private GUIFactory guiFactory = new GUIFactory();
    private final Mode mode;
    private Patient existingPatient = null;

    TextField firstName;
    TextField lastName;
    TextField socialSecurityNumber;
    TextField diagnose;
    TextField generalPractitioner;

    //the variable below is used to store the error message and is handled in the controller class
    private String errorMessage = null;

    /**
     * This constructor is used to create a new patient
     */
    public PatientDialog() {
        super();
        mode = Mode.NEW;
        createPatient();
    }

    /**
     * This constructor is used to edit a selected patient
     * @param patient
     */
    public PatientDialog(Patient patient) {
        super();
        mode = Mode.EDIT;
        existingPatient = patient;
        createPatient();
    }

    /**
     * creates the GUI for adding or editing a patient
     */
    private void createPatient() {
        //decides the title to be shown depending on the mode
        switch (mode){
            case EDIT:
                setTitle("Edit Selected Patient");
                break;

            case NEW:
                setTitle("Patient Details - Add");
                break;

            default:
                setTitle("UNKNOWN MODE...");
        }

        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        GridPane grid = (GridPane) guiFactory.getNode("gridpane");
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        firstName = (TextField) guiFactory.getNode("Textfield");
        firstName.setPromptText("First Name");

        lastName = (TextField) guiFactory.getNode("Textfield");
        lastName.setPromptText("Last Name");

        socialSecurityNumber = (TextField) guiFactory.getNode("Textfield");
        socialSecurityNumber.setPromptText("Social Security Number");

        diagnose = (TextField) guiFactory.getNode("Textfield");
        diagnose.setPromptText("Diagnose");

        generalPractitioner = (TextField) guiFactory.getNode("Textfield");
        generalPractitioner.setPromptText("General Practitioner");

        //if the mode is EDIT, then the selected patient's information is displayed to the user
        if (mode == Mode.EDIT){
            firstName.setText(existingPatient.getFirstName());
            lastName.setText(existingPatient.getLastName());
            socialSecurityNumber.setText(existingPatient.getSocialSecurityNumber());
            diagnose.setText(existingPatient.getDiagnose());
            generalPractitioner.setText(existingPatient.getGeneralPractitioner());
        }

        Label fN = (Label) guiFactory.getNode("label");
        fN.setText("First Name: ");
        Label lN = (Label) guiFactory.getNode("label");
        lN.setText("Last Name: ");
        Label lSSN = (Label) guiFactory.getNode("label");
        lSSN.setText("Social Security Number: ");
        Label lD = (Label) guiFactory.getNode("label");
        lD.setText("Diagnose: ");
        Label lGP = (Label) guiFactory.getNode("label");
        lGP.setText("General Practitioner: ");


        grid.add(fN, 0, 0);
        grid.add(firstName, 1, 0);

        grid.add(lN, 0, 1);
        grid.add(lastName, 1, 1);

        grid.add(lSSN, 0, 2);
        grid.add(socialSecurityNumber, 1, 2);

        grid.add(lD, 0, 3);
        grid.add(diagnose, 1, 3);

        grid.add(lGP, 0, 4);
        grid.add(generalPractitioner, 1, 4);

        getDialogPane().setContent(grid);

        //return the data from the user's input
        setResultConverter((ButtonType button) -> {
            Patient result = null;
            if (button == ButtonType.OK) {
                if (mode == Mode.NEW) {
                    try {
                        result = new Patient(socialSecurityNumber.getText(), firstName.getText(),
                                    lastName.getText(), diagnose.getText(), generalPractitioner.getText());
                    } catch (Exception e) {
                        errorMessage = e.getMessage();
                    }

                } else if (mode == Mode.EDIT){

                    try {
                        existingPatient.setSocialSecurityNumber(socialSecurityNumber.getText());
                        existingPatient.setFirstName(firstName.getText());
                        existingPatient.setLastName(lastName.getText());
                        existingPatient.setDiagnose(diagnose.getText());
                        existingPatient.setGeneralPractitioner(generalPractitioner.getText());
                        result = existingPatient;
                    } catch (Exception e){
                        errorMessage = e.getMessage();
                    }

                }
            }
            return result;
        });
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}
