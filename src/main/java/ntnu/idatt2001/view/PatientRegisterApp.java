package ntnu.idatt2001.view;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ntnu.idatt2001.controller.Controller;
import ntnu.idatt2001.model.CSVFileHandling;
import ntnu.idatt2001.model.GUIFactory;
import ntnu.idatt2001.model.Patient;
import ntnu.idatt2001.model.PatientRegister;

public class PatientRegisterApp extends Application {

    private CSVFileHandling csvFileHandling;

    private VisualGUI visualGUI; //a factory class for creating a custom menubar, toolbar and tableview
    private GUIFactory guiFactory; // a factory class for initializing GUI elements that inherit from Node

    private Controller controller;
    private PatientRegister patientRegister;
    private ObservableList<Patient> patientObservableList;

    private TableView<Patient> tableView;

    /**
     * The variables mentioned inside this method are initialized when the application opens.
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
        super.init();

        csvFileHandling = new CSVFileHandling();

        guiFactory = new GUIFactory();
        visualGUI = new VisualGUI();
        controller = new Controller();
        patientRegister = new PatientRegister();
        patientObservableList = FXCollections.observableArrayList(patientRegister.getPatients());
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Patient Register");

        BorderPane mainWindow = (BorderPane) guiFactory.getNode("BorderPane");
        //a custom tableview is made from the class VisualGUI
        tableView = (TableView) visualGUI.getNode("Custom TableView");
        tableView.setItems(patientObservableList);
        mainWindow.setCenter(tableView);

        //the top of the application shall consist of a menubar and a toolbar
        VBox topBars = (VBox) guiFactory.getNode("VBox");
        topBars.getChildren().addAll(controlsForMenuBar(), controlsForToolBar());
        mainWindow.setTop(topBars);

        Scene scene = new Scene(mainWindow, 900, 400);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The method creates a custom menubar and sets actions to the options the menubar provides.
     * @return a menubar
     */
    public MenuBar controlsForMenuBar() {
        //creates a menubar from a factory class (VisualGUI)
        MenuBar menuBar = (MenuBar) visualGUI.getNode("Custom MenuBar");

        //sets the actions to the load and save data options in the file menu
        menuBar.getMenus().get(0).getItems().get(0).setOnAction(event ->
                controller.loadData(patientRegister, csvFileHandling, this));
        menuBar.getMenus().get(0).getItems().get(1).setOnAction(event ->
                controller.saveData(patientRegister, csvFileHandling));

        //sets an action to the exit option in the file menu
        menuBar.getMenus().get(0).getItems().get(3).setOnAction(event -> System.exit(0));

        //sets the actions to the add, edit and remove options in the edit menu
        menuBar.getMenus().get(1).getItems().get(0)
                .setOnAction(event -> controller.addPatient(patientRegister, this));
        menuBar.getMenus().get(1).getItems().get(1)
                .setOnAction(event -> controller.editPatient(tableView.getSelectionModel().getSelectedItem(), this));
        menuBar.getMenus().get(1).getItems().get(2)
                .setOnAction(event -> controller.removePatient(
                        tableView.getSelectionModel().getSelectedItem(), patientRegister, this));

        //sets an action to the about option in the help menu
        menuBar.getMenus().get(2).getItems().get(0)
                .setOnAction(event -> controller.showInformationDialog());

        return menuBar;
    }

    /**
     * The method creates a custom toolbar, and sets actions to the options in the toolbar
     * @return a toolbar
     */
    public ToolBar controlsForToolBar() {
        //creates a toolbar from a factory class (VisualGUI)
        ToolBar toolBar = (ToolBar) visualGUI.getNode("Custom ToolBar");

        //sets the actions to the add, edit, and remove icons
        toolBar.getItems().get(0).setOnMouseClicked(event -> controller.addPatient(patientRegister, this));
        toolBar.getItems().get(1).setOnMouseClicked(event ->
                controller.editPatient(tableView.getSelectionModel().getSelectedItem(), this));
        toolBar.getItems().get(2).setOnMouseClicked(event -> controller.removePatient(
                tableView.getSelectionModel().getSelectedItem(), patientRegister, this));

        return toolBar;
    }

    /**
     * a method that updates the observable list so that the tableview gets updated
     */
    public void updateObservableList() {
        patientObservableList.setAll(patientRegister.getPatients());
    }

}
