package ntnu.idatt2001.model;

import java.util.ArrayList;

public class PatientRegister {

    private ArrayList<Patient> patients; //stores all patients in an arraylist

    /**
     * the list patients is initialized
     */
    public PatientRegister(){
        patients = new ArrayList<>();
    }

    /**
     * The method adds a patient if it does not exist from before
     * @param patient
     * @return true if patient is added, false otherwise
     */
    public boolean addPatient(Patient patient){
        if (!patients.contains(patient) && patient != null){
            patients.add(patient);
            return true;
        }
        return false;
    }

    /**
     * The method is used to add a list of patients at once instead of adding a patient one by one
     * @param patients
     */
    public void addPatientList(ArrayList<Patient> patients){
        for (Patient patient: patients){
            if (!this.patients.contains(patient) && patient != null){
                this.patients.add(patient);
            }
        }
    }

    /**
     * The method removes a patient
     * @param patient
     * @return true if patient is removed, false if not
     */
    public boolean removePatient(Patient patient){
        return patients.remove(patient);
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }


    @Override
    public String toString() {
        String listText = "";
        for (Patient patient: patients) {
            listText += patient + "\n";
        }
        return listText;
    }

}
