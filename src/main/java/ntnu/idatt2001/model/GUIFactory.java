package ntnu.idatt2001.model;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class GUIFactory {

    /**
     * The method takes in a type of GUI element, and returns the element if found.
     * This method only handles GUI elements that inherit from Node
     * @param type the type of GUI element
     * @return a GUI element of the given type, else returns null the given type does not match the given choices
     */
    public Node getNode(String type){
        if (type == null){
            return null;
        }

        if (type.equalsIgnoreCase("menubar")){
            return new MenuBar();
        }
        if (type.equalsIgnoreCase("toolbar")){
            return new ToolBar();
        }
        if (type.equalsIgnoreCase("tableview")){
            return new TableView<>();
        }
        if (type.equalsIgnoreCase("borderpane")){
            return new BorderPane();
        }
        if (type.equalsIgnoreCase("gridpane")){
            return new GridPane();
        }
        if (type.equalsIgnoreCase("vbox")){
            return new VBox();
        }
        if (type.equalsIgnoreCase("label")){
            return new Label();
        }
        if (type.equalsIgnoreCase("button")){
            return new Button();
        }
        if (type.equalsIgnoreCase("textfield")){
            return new TextField();
        }
        if (type.equalsIgnoreCase("imageview")){
            return new ImageView();
        }

        return null;
    }
}
