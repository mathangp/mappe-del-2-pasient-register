package ntnu.idatt2001.model;

import java.io.*;
import java.util.ArrayList;

/**
 * This class handles reading from a file and writing to a file
 */
public class CSVFileHandling {

    private ArrayList<Patient> patientsFromFile = new ArrayList<>(); //stores all patients of the type Patient
    private ArrayList<String> dataPatients = new ArrayList<>(); //stores all patients in the form of text

    private String exceptionThrown = null; //this is used in the test class to  the methods of this class

    /**
     * The method reads the given file line for line, and saves every line in the dataPatients list
     * @param fileName
     */
    public void readFileCSV(String fileName) {
        try (BufferedReader sc = new BufferedReader(new FileReader(fileName))) {
            String line = sc.readLine(); //skip the first line because it is the title describing about the type of information
            while ((line = sc.readLine()) != null){ //this loop will continue until all contents of the file are read
                dataPatients.add(line);
            }

        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
            exceptionThrown = e.getMessage();
        } catch (IOException e){
            System.out.println(e.getMessage());
            exceptionThrown = e.getMessage();
        } catch (Exception e){
            System.out.println(e.getMessage());
            exceptionThrown = e.getMessage();
        }
    }

    /**
     * The method reads the file using the method readFileCSV,
     * so that we have a list of patients in String type (the list is called dataPatients)
     * We use the dataPatients list, and iterate through each item and convert it to a
     * Patient object and save it to a new list called patientsFromFile
     *
     * @param fileName
     * @return a list with all the patients that were in the given file
     */
    public ArrayList formatDataToList(String fileName) {
        readFileCSV(fileName);

        for (String data: dataPatients) {
            Patient patient = null;
            String temp[] = data.split(";");
            if (temp.length == 4){
                patient = new Patient(temp[3], temp[0], temp[1], "", temp[2]);
            } else {
                patient = new Patient(temp[3], temp[0], temp[1], temp[4], temp[2]);
            }

            patientsFromFile.add(patient);
        }

        return patientsFromFile;
    }

    /**
     * The method takes in a list of patients and writes the list to the given file.
     * @param patients
     * @param fileName
     */
    public void writeToFile(ArrayList<Patient> patients, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            //We write the title of the file which describes the type of information that will be stored
            bufferedWriter.write("firstName;lastName;generalPractitioner;socialSecurityNumber;diagnose");

            //We go through each item in the list and convert it to a String object and add it to the file
            for (Patient patient: patients){
                String text = patient.getFirstName() + ";" + patient.getLastName() + ";" +
                        patient.getGeneralPractitioner() + ";" + patient.getSocialSecurityNumber() + ";" +
                        patient.getDiagnose();
                bufferedWriter.newLine();
                bufferedWriter.write(text);
            }

        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
            exceptionThrown = e.getMessage();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            exceptionThrown = e.getMessage();
        } catch (Exception e){
            System.out.println(e.getMessage());
            exceptionThrown = e.getMessage();
        }
    }

    public String getExceptionThrown() {
        return exceptionThrown;
    }
}
