package ntnu.idatt2001.model;

import java.util.Objects;

/**
 * This is a mutable class
 */
public class Patient {

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnose;
    private String generalPractitioner;

    /**
     * The constructur checks for any invalid input before setting values to the attributes.
     * @param socialSecurityNumber
     * @param firstName
     * @param lastName
     * @param diagnose
     * @param generalPractitioner
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName,
                   String diagnose, String generalPractitioner) {
        nameCheck(firstName);
        nameCheck(lastName);
        nameCheck(generalPractitioner);
        securityNumberCheckAndSet(socialSecurityNumber);

        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnose = diagnose;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * A help method to check if the social security number is valid
     * Social Security Number is only valid when it contains no letters and only contains 11 numbers.
     * When a social security number has only 10 numbers, it might have lost the 0 in front if the
     * number when written on a file so a check for this is also added here.
     * @param socialSecurityNumber
     */
    private void securityNumberCheckAndSet(String socialSecurityNumber) {
        if (socialSecurityNumber.matches("[0-9]+") && socialSecurityNumber.length() == 10 && !socialSecurityNumber.startsWith("0")){
            this.socialSecurityNumber = "0"+socialSecurityNumber;
        } else if (!socialSecurityNumber.matches("[0-9]+") || socialSecurityNumber.length() != 11){
            throw new IllegalArgumentException("Social Security Number should be 11 numbers and no letters.");
        } else {
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    /**
     * A help method to check if the first name, last name and general practitioner name is valid
     * @param name
     */
    private void nameCheck(String name) {
        if (name.isEmpty()){
            throw new IllegalArgumentException("Please fill out valid names. \n" +
                    "Check if the first name, last name and general practitioner \nis filled out");
        }
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        securityNumberCheckAndSet(socialSecurityNumber);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        nameCheck(firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        nameCheck(lastName);
        this.lastName = lastName;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        nameCheck(generalPractitioner);
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Two patients are equal only if their social security number is equal.
     * @param o an object that will be used to compare with another object
     * @return true if two patients are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient: " +
                "Social Security Number = " + socialSecurityNumber +
                ", First Name = " + firstName +
                ", Last Name = " + lastName +
                ", Diagnose = " + diagnose +
                ", General Practitioner = " + generalPractitioner;
    }
}
