package ntnu.idatt2001.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import ntnu.idatt2001.model.Patient;
import ntnu.idatt2001.model.PatientRegister;
import ntnu.idatt2001.model.CSVFileHandling;
import ntnu.idatt2001.view.PatientDialog;
import ntnu.idatt2001.view.PatientRegisterApp;
import ntnu.idatt2001.view.SaveLoadDialog;

import java.io.*;
import java.util.Optional;

/**
 * The is the controller class for PatientRegisterApp.
 * The actions adding, editing and removing a patient and loading and saving data are handled here
 */
public class Controller {

    private String version = "v1.0-SNAPSHOT";

    /**
     * The method adds a new Patient filled out by the user to the patient register and
     * the table that the user views gets updated.
     * If the user enters invalid information, the error is shown to the user.
     * If the user tries to add a patient that already exists, the user will get feedback about this.
     * @param patientRegister
     * @param app
     */
    public void addPatient(PatientRegister patientRegister, PatientRegisterApp app) {
        PatientDialog addDialog = new PatientDialog();
        Optional<Patient> result = addDialog.showAndWait(); //the input from the user

        if (result.isPresent()){
            Patient newPatient = result.get();
            if (!patientRegister.addPatient(newPatient)){ //checks if patient already exists
                showInformationError("Patient exists", "Patient already exists");
            } else {
                patientRegister.addPatient(newPatient);
                app.updateObservableList(); //updates the tableview
            }

        } else if (addDialog.getErrorMessage() != null) {
            // if the error message is not null, then it means that an Exception has been thrown in addDialog
            showInformationError("Error", addDialog.getErrorMessage());
            addPatient(patientRegister, app);
        }

    }

    /**
     * If the user has selected a patient, the edit method can be called and allows to the user to edit
     * the patient's details. If the user writes invalid information, the error is shown to the user.
     * If the user has not selected a patient, and calls the edit method, the user will be asked to
     * select a patient from the tableview.
     * @param selectedPatient
     * @param app
     */
    public void editPatient(Patient selectedPatient, PatientRegisterApp app) {
        if (selectedPatient == null){
            showPleaseSelectItemDialog(); //if the user has not selected a patient
        } else {
            PatientDialog employeeDialog = new PatientDialog(selectedPatient);
            employeeDialog.showAndWait();
            // if the error message is not null, then it means that an Exception has been thrown in employeeDialog
            if (employeeDialog.getErrorMessage() != null){
                showInformationError("Error", employeeDialog.getErrorMessage()); //shows what type of error it is
                editPatient(selectedPatient, app); //allows the user to correct the information
            }
        }
        app.updateObservableList(); //updates the tableview
    }

    /**
     * If the user attempt to delete a patient without selecting a patient, the user will be asked
     * to select a patient.
     * If the user has selected a patient and attempts to delete the patient, a delete confirmation
     * dialog will pop out and lets the user click the OK to delete the patient, CANCEL if not.
     * If something has gone wrong when deleting the patient, the user will get feedback about this.
     * @param selectedPatient
     * @param patientRegister
     * @param app
     */
    public void removePatient(Patient selectedPatient, PatientRegister patientRegister, PatientRegisterApp app) {
        if (selectedPatient == null){
            showPleaseSelectItemDialog();
        } else {
            if (showDeleteConfirmationDialog()){ //if true, patient will be deleted
                //if removing a patient returns false, then something has gone wrong, and the user will get feedback about this
                if (!patientRegister.removePatient(selectedPatient)){
                    showInformationError("Remove Error", "Something has gone wrong. \nTry again");
                } else {
                    patientRegister.removePatient(selectedPatient);
                    app.updateObservableList(); //updates the tableview
                }
            }
        }
    }

    /**
     * When the user clicks on "Export to .CSV/Save data" this method is carried out
     * The file and location input from the user is checked, and if everything seems correct, the file
     * will be loaded to the tableview. If the input is wrong, the user gets feedback about this through
     * a dialog box.
     * The exceptions that might occur when handling files is also handled in this method.
     * @param patientRegister
     * @param csvFileHandling
     */
    public void saveData(PatientRegister patientRegister, CSVFileHandling csvFileHandling) {
        SaveLoadDialog saveDialog = new SaveLoadDialog(SaveLoadDialog.Mode.SAVE);
        Optional<String> result = saveDialog.showAndWait();

        if (result.isPresent()){
            String fileAndLocation = result.get(); // A text with both the filename and location
            String temp[] = fileAndLocation.split(";"); //the filename and location is split to two items in a list

            //if the temp list has length 0, or if the filename does not end with .csv,
            // the user is given feedback about this
            if (temp.length == 0 || temp.length == 1 || !temp[0].endsWith(".csv")){
                showInformationError("Cannot save file","File name must end with '.csv' and a correct " +
                        "location must be given");
                //goes back to the dialog box where the user can fill out the filename and location
                saveData(patientRegister, csvFileHandling);
            } else {
                File newFile = new File(temp[1] + "/" + temp[0]); //a file is created from the user's input
                //if the file does not exist, then a new file that can be written on is created,
                // and the tableview data is written to the new file.
                if (!newFile.exists()){
                    try {
                        BufferedWriter writer = new BufferedWriter(new FileWriter(temp[1] + "/" + temp[0]));
                        csvFileHandling.writeToFile(patientRegister.getPatients(), temp[1] + "/" + temp[0]);
                        writer.close();
                    } catch (Exception e){
                        //error message if something goes wrong
                        showInformationError("An error has occurred.", e.getMessage());
                        saveData(patientRegister, csvFileHandling);
                    }

                }else if (newFile.exists()){
                    //if the given file information exists, then the user it asked to confirm
                    // overwriting the data to the existing file
                    if (showOverwriteConfirmationDialog()){
                        try {
                            csvFileHandling.writeToFile(patientRegister.getPatients(), temp[1] + "/" + temp[0]);
                        } catch (Exception e){
                            //error message if something goes wrong
                            showInformationError("An error has occurred.", e.getMessage());
                        }

                    } else {
                        saveData(patientRegister, csvFileHandling);
                    }
                }
            }
        }
    }

    /**
     * When user clicks on "Import from .CSV / Load Data", the following method will be carried out.
     * The file and location input from the user is checked, and if everything seems correct, the file
     * will be loaded to the tableview. If the input is wrong, the user gets feedback about this through
     * a dialog box.
     * The exceptions that might occur when handling files is also handled in this method.
     * @param patientRegister
     * @param csvFileHandling
     * @param app
     */
    public void loadData(PatientRegister patientRegister, CSVFileHandling csvFileHandling, PatientRegisterApp app) {
        SaveLoadDialog loadDialog = new SaveLoadDialog(SaveLoadDialog.Mode.LOAD);
        Optional<String> result = loadDialog.showAndWait();

        if (result.isPresent()){
            String fileAndLocation = result.get(); // A text with both the filename and location
            String temp[] = fileAndLocation.split(";"); //the filename and location is split to two items in a list

            //if the temp list has length 0, or if the filename does not end with .csv,
            // the user is given feedback about this
            if (temp.length == 0 || temp.length == 1 || !temp[0].endsWith(".csv")){
                showInformationError("Cannot open file",
                        "The chosen file type is not valid or the specified file location is wrong.\n" +
                                "Please choose another file by clicking on " +
                                "\nSelect File and the OK button or Cancel the operation");
                //goes back to the dialog box where the user can fill out the filename and location, or select a file
                loadData(patientRegister, csvFileHandling, app);

            } else {
                File file = new File(temp[1] + "/" + temp[0]); //a file is created from the user's input
                //if the file exists, the data from the file is loaded to the table.
                if (file.exists()){
                    try {
                        patientRegister.addPatientList(csvFileHandling.formatDataToList(temp[1] + "/" + temp[0]));
                        app.updateObservableList();
                    } catch (Exception e){
                        //error message if something goes wrong
                        showInformationError("An error has occurred.", e.getMessage());
                    }

                } else if (!file.exists()){
                    showInformationError("File does not exist or invalid file path","The chosen file type is not valid or the " +
                            "specified file location is wrong.\n" + "Please choose another file by clicking on " +
                            "\nSelect File and the OK button or Cancel the operation");
                    loadData(patientRegister, csvFileHandling, app);
                }
            }
        }
    }

    /**
     * A dialog box that pops up whenever the user wants to delete a patient
     * in order to double check of the user really wants to delete the patient.
     * @return true if user chooses the OK button, false otherwise.
     */
    public boolean showDeleteConfirmationDialog(){
        boolean deleteConfirmed = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Delete Patient's Information?");
        alert.setContentText("Are you sure you want to delete this patient's information?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent()){
            deleteConfirmed = (result.get() == ButtonType.OK);
        }
        return deleteConfirmed;
    }

    /**
     * A dialog box for when the user clicks on "Help" -> "About" option.
     * This will show information about the application.
     */
    public void showInformationDialog(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patients Register\n" + version);
        alert.setContentText("Application made for Mappe Del 2.\nMade by Mathangi Pushparajah\n04/05/2021");

        Optional<ButtonType> result = alert.showAndWait();
    }

    /**
     * A dialog box that pops up when the user tries to save data to an existing file.
     * This will ask the user if he/she wants to overwrite the file or not.
     * @return true, of user clicks on OK to overwrite the file, false otherwise.
     */
    public boolean showOverwriteConfirmationDialog(){
        boolean overwriteConfirmed = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Overwrite confirmation");
        alert.setHeaderText("Overwrite the existing file?");
        alert.setContentText("A file with same name exists. Do you want to overwrite it?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent()){
            overwriteConfirmed = (result.get() == ButtonType.OK);
        }
        return overwriteConfirmed;
    }

    /**
     *A dialog box that pops out and gives information about the user, if the given information about
     * a file is wrong.
     * @param message
     */
    public void showInformationError(String header, String message){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.getDialogPane().setPrefHeight(200);
        alert.setTitle("Error");
        alert.setHeaderText(header);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
    }

    /**
     * A dialog box that pops and gives feedback to user if the user has not selected
     * a patient from the tableview to edit or remove.
     */
    public void showPleaseSelectItemDialog(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Information");
        alert.setHeaderText("No patients are selected");
        alert.setContentText("No patient is selected from the table. \n" +
                "Please select a patient from the table");
        alert.showAndWait();
    }
}
