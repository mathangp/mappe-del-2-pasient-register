package ntnu.idatt2001.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import java.io.File;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CSVFileHandlingTest {

    private CSVFileHandling csvFileHandling = new CSVFileHandling();
    private String testReadFile = "src/test/resources/Patients.csv"; //used to test reading a file

    @Nested
    public class readFileMethodTest {

        @Test
        @DisplayName("Read file from a valid file path")
        public void readFileFromValidFile() {
            csvFileHandling.readFileCSV(testReadFile);
            //if reading the file has not caused any exceptions to be thrown the variable storing-
            //-the exception message should be null
            assertNull(csvFileHandling.getExceptionThrown());
        }

        @Test
        @DisplayName("Read the contents of the file correctly")
        public void readFileCorrectly(){
            try {
                ArrayList<Patient> content = csvFileHandling.formatDataToList(testReadFile);
                //the first line of the file is ignored
                //If the file is read correcting, the number of items in the content list should be 104
                assertEquals(104, content.size());

            } catch (Exception e) {
                fail("Something went wrong when reading the file");
            }
        }

        @Test
        @DisplayName("Read file from a file path that does not exist")
        public void readFileFromInvalidFile(){
            csvFileHandling.readFileCSV("Testing");
            //a FileNotFoundException should occur, and the following message will be thrown:
            //"Testing (The system cannot find the file specified)"
            assertEquals("Testing (The system cannot find the file specified)", csvFileHandling.getExceptionThrown());
        }
    }

    //only positive tests
    @Nested
    public class writeToFileMethodTest {

        @Test
        @DisplayName("Create a new file and write to it")
        public void createAndWriteToNewFile() {
            ArrayList<Patient> patients = new ArrayList<>();
            patients.add(new Patient("12345678912", "Nemo", "Fish", "", "Freddy"));
            patients.add(new Patient("12345678913", "Nemy", "Fish", "", "Freddy"));

            //this variable stores the file path. Used to test writing to a new file
            String testWriteFile = "src/test/resources/newFile.csv";
            csvFileHandling.writeToFile(patients, testWriteFile);

            File testCreatedFile = new File(testWriteFile);
            //if the testCreatedFile exists, then it means it has been created successfully
            assertTrue(testCreatedFile.exists());
        }

        @Test
        @DisplayName("Check if content is written correctly to a new file")
        public void checkIfContentIsWrittenCorrectlyToNewFile() {
            //the list that will be written to a file
            ArrayList<Patient> patients = new ArrayList<>();
            patients.add(new Patient("12345678912", "Nemo", "Fish", "", "Freddy"));
            patients.add(new Patient("12345678913", "Nemy", "Fish", "", "Freddy"));

            //this variable stores the filepath used to test if the content is written correctly to file
            String testWriteFile = "src/test/resources/checkFile.csv";
            csvFileHandling.writeToFile(patients, testWriteFile); //new file is created and written on

            ArrayList<Patient> content = csvFileHandling.formatDataToList(testWriteFile);
            //if the content has been correctly written to checkFile.csv, then the the number of-
            //-items in content is 2.
            assertEquals(2, content.size());
        }
    }
}
