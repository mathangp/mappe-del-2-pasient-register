package ntnu.idatt2001.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {

    private PatientRegister patientRegister = new PatientRegister();

    //data that needs to be created before the tests
    @BeforeEach
    public void runData() throws Exception {
        patientRegister.addPatient(new Patient("12341234123",
                "Hola", "hola", "", "Doctor"));
        patientRegister.addPatient(new Patient("19283847465",
                "Triv", "Lie", "", "Kate"));
    }

    //testing the addPatient method in the PatientRegister class
    @Nested
    public class addPatientMethod {

        @Test
        @DisplayName("Add patients with invalid social security number")
        public void addPatientWithInvalidSSN(){
            //the Social Security number contains letters
            assertThrows(IllegalArgumentException.class,
                    () -> patientRegister.addPatient(new Patient("qwe34123412",
                            "Hola", "hola", "", "Doctor")));

            //the Social Security number contains 9 numbers
            assertThrows(IllegalArgumentException.class,
                    () -> patientRegister.addPatient(new Patient("341232311",
                            "Hela", "hela", "", "Doctor")));


        }

        @Test
        @DisplayName("Add patients with invalid names")
        public void addPatientWithInvalidNames(){
            //The first name is invalid
            assertThrows(IllegalArgumentException.class,
                    () -> patientRegister.addPatient(new Patient("34123412123",
                            "", "hola", "", "Doctor")));

            //the last name is invalid
            assertThrows(IllegalArgumentException.class,
                    () -> patientRegister.addPatient(new Patient("34121231167",
                            "Hola", "", "", "Doctor")));

            //the general practitioner name is invalid
            assertThrows(IllegalArgumentException.class,
                    () -> patientRegister.addPatient(new Patient("12221231167",
                            "Emma", "emma", "", "")));


        }

        @Test
        @DisplayName("Adding a patient")
        public void addNewPatient() throws Exception {
            Patient patient = new Patient("11323234120",
                    "Heile", "Karlos", "", "Doctor");
            assertTrue(patientRegister.addPatient(patient));
        }

        @Test
        @DisplayName("Adding a patient that already exists")
        public void addExistingPatient() throws Exception {
            Patient patient = new Patient("12341234123",
                    "Hola", "hola", "", "Doctor");
            assertFalse(patientRegister.addPatient(patient));
        }

    }

    //testing the removePatient in PatientRegister class
    @Nested
    public class removePatientMethod {

        @Test
        @DisplayName("Remove a patient that has been registered")
        public void removeExistingPatient() throws Exception {
            Patient patient = new Patient("12341234123",
                    "Hola", "hola", "", "Doctor");
            assertTrue(patientRegister.removePatient(patient));
        }

        @Test
        @DisplayName("Remove a patient that has not been registered")
        public void removeNonExistingPatient() throws Exception {
            Patient patient = new Patient("11341234120",
                    "Hein", "Karla", "", "Doctor");
            assertFalse(patientRegister.removePatient(patient));
        }
    }
}
